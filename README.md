# Mosaico Fractal Web #

O programa Mosaico Fractal tem como objetivo preencher um determinado espaço utilizando estampas que possuem uma área que respeitam uma lei de potência, na qual utiliza a função zeta de Hurwitz para este propósito, assim permitindo que estas estampas sejam inseridas no espaço ad infinitum, e por fim apresentando um resultado que se assemelha a um fractal geométrico, sendo também esteticamente agradável.

### Instruções ###
* Acesse o [site do programa](http://gior.hol.es/mosaico);
* Escolha a estampa que deseja utilizar para preencher um espaço;
* Escolha o espaço de preenchimento (fundo);
* Escolha os valores de C e N (para saber mais sobre suas influências, leie sobre o programa em meu [TCC](http://gior.hol.es/docs/degregoriis2017utilizacao.pdf));
* Clique para iniciar;
* Se desejar salvar a imagem, clique em salvar (Obs: não funciona bem no Safari).

Caso tenha interesse na versão desktop, acesse o [repositório](https://bitbucket.org/giordanna/mosaico-fractal/).

### Dúvidas ###

* Fale com a responsável pelo repositório ou;
* Envie um email para: giordanna.gregoriis@icen.ufpa.br